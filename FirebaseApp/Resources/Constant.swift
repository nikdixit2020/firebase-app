//
//  Constant.swift
//  FirebaseApp
//
//  Created by Nikhil on 18/10/20.
//  Copyright © 2020 Dixit. All rights reserved.
//

import Foundation
import UIKit

struct messages
{
    let emailempty = "Please enter email"
    let email_valid = "Please enter valid email id"
    let password = "Please enter password"
    let password_valid = "Password length should be 6 to 15 digits"
}

//# E-mail Validation

func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}



