//
//  LoginView.swift
//  FirebaseApp
//
//  Created by Nikhil on 18/10/20.
//  Copyright © 2020 Dixit. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
class LoginView: UIViewController {

    @IBOutlet weak var sign_up_btn: UIButton!
    lazy var errormessage = String()
    @IBOutlet weak var Sign_btn: UIButton!
    @IBOutlet weak var Password_txt: UITextField!
    @IBOutlet weak var Email_txt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Email_txt.keyboardType = .emailAddress
        self.Password_txt.isSecureTextEntry = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Sign_btn_action(_ sender: UIButton)
    {
        if self.SignInValidation() == false
        {
            self.alert_action(title: "", message: self.errormessage)
        }
        else
        {
            self.Firebase_SignIn(email: self.Email_txt.text!, password: self.Password_txt.text!)
        }
    }
    
    func Firebase_SignIn(email:String,password:String)
    {
        let signupManager = Auth.auth()
        
        signupManager.signIn(withEmail: email, password: password) { (result, error) in
            
            if error != nil
            {
               self.alert_action(title: "", message: error?.localizedDescription ?? "")
            }
            else
            {
                
                let email = result?.user.email ?? ""
                let user_id = result?.user.refreshToken ?? ""
                let display_name = result?.user.displayName ?? ""
                let prover_id = result?.user.uid ?? ""
                
                let login_arr = ["email":email,"refresh_token":user_id,"display_name":display_name,"uid":prover_id]
                print(login_arr)
            }
        }
    }
    
    @IBAction func sing_up_btn_Action(_ sender: Any)
    {
        let register = self.storyboard?.instantiateViewController(withIdentifier: "RegisterView") as! RegisterView
        register.modalPresentationStyle = .overFullScreen
        register.modalTransitionStyle = .flipHorizontal
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    func signIn(email: String, pass: String, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionBlock(false)
            } else {
                completionBlock(true)
            }
        }
    }
    
    func SignInValidation() -> Bool
    {
        if self.Email_txt.text?.isEmpty == true
        {
            self.errormessage = messages().emailempty
            return false
        }
        if isValidEmail(self.Email_txt.text!) == false
        {
            self.errormessage = messages().email_valid
            return false
        }
        if self.Password_txt.text?.isEmpty == true
        {
            self.errormessage = messages().password
            return false
        }
        if (self.Password_txt.text?.count)! < 5 || (self.Password_txt.text?.count)! > 15
        {
            self.errormessage = messages().password_valid
            return false
        }
        
        self.errormessage = ""
        return true
    }
    
    func alert_action(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle:
            UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
           }))

           self.present(alert, animated: true, completion:nil)
    }
}
