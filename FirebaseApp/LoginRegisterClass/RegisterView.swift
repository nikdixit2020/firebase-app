//
//  RegisterView.swift
//  FirebaseApp
//
//  Created by Nikhil on 18/10/20.
//  Copyright © 2020 Dixit. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class RegisterView: UIViewController {
    
    @IBOutlet weak var Sing_in_btn: UIButton!
    lazy var errormessage = String()
    @IBOutlet weak var Sign_btn: UIButton!
    @IBOutlet weak var Password_txt: UITextField!
    @IBOutlet weak var Email_txt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Email_txt.keyboardType = .emailAddress
        self.Password_txt.isSecureTextEntry = true
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Sign_btn_action(_ sender: UIButton)
    {
        if self.SignUpValidation() == false
        {
            self.alert_action(title: "", message: self.errormessage)
        }
        else
        {
            self.Firebase_registration(email: self.Email_txt.text!, password: self.Password_txt.text!)
        }
    }
    
    @IBAction func SignIn_btn_action(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func Firebase_registration(email:String,password:String)
    {
        let signupManager = Auth.auth()
        
        signupManager.createUser(withEmail: email, password: password) { (result, error) in
            
            if error != nil
            {
               self.alert_action(title: "", message: error?.localizedDescription ?? "")
            }
            else
            {
                
                let email = result?.user.email ?? ""
                let user_id = result?.user.refreshToken ?? ""
                let display_name = result?.user.displayName ?? ""
                let prover_id = result?.user.uid ?? ""
                
                let login_arr = ["email":email,"refresh_token":user_id,"display_name":display_name,"uid":prover_id]
                print(login_arr)
                
            }
        }
    }
    
    
    func SignUpValidation() -> Bool
    {
        if self.Email_txt.text?.isEmpty == true
        {
            self.errormessage = messages().emailempty
            return false
        }
        if isValidEmail(self.Email_txt.text!) == false
        {
            self.errormessage = messages().email_valid
            return false
        }
        if self.Password_txt.text?.isEmpty == true
        {
            self.errormessage = messages().password
            return false
        }
        if (self.Password_txt.text?.count)! < 5 || (self.Password_txt.text?.count)! > 15
        {
            self.errormessage = messages().password_valid
            return false
        }
        
        self.errormessage = ""
        return true
    }
    
    func alert_action(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle:
            UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion:nil)
    }
}
